#ifndef SRC_VECTOR_MESH_2_H_
#define SRC_VECTOR_MESH_2_H_

#include "Vector3.h"
#include "utils.h"

#define VM2_MIN_DIFF 0.0001

/**
 * Класс двухмерной векторной сетки
 */
class VectorMesh2
{
public:
	VectorMesh2(double dx, double dy,
				      double ax, double bx,
				      double ay, double by);
	~VectorMesh2(void);

	/**
	 * Возвращает индекст в псевдотрехмерном массиве
	 * @param  i индекс узла по x
	 * @param  j индекс узла по y
	 * @return  индекс в псевдодвухмерном массиве
	 */
	int getIndex(int i, int j)
	{
		return i + j*nx;
	}

	/**
	 * Попадают ли указанные индексы в диапазон индексов узлов сетки
	 * @param  i индекс узла по x
	 * @param  j индекс узла по y
	 * @return
	 */
	bool indexInRange(int i, int j)
	{
		return inRange(i, 0, nx) && inRange(j, 0, ny);
	}

	/**
	 * Возвращает значение узла (i,j)
	 * @param  i
	 * @param  j
	 * @return
	 */
	Vector3 get(int i, int j)
	{
		return indexInRange(i, j) ? data[getIndex(i, j)] : Vector3(0, 0, 0);
	}

	/**
	 * Задает значение узда (i,j)
	 * @param i
	 * @param j
	 * @param vx - значение x компоненты вектора узла
	 * @param vy - значение y компоненты вектора узла
	 */
	void set(int i, int j, double vx, double vy)
	{
		if (indexInRange(i, j))
			data[getIndex(i, j)] = Vector3(vx, vy, 0);
	}

	/**
	 * Возвращает интерполированное значение из указанной координаты
	 * @param  x - координата на сетке
	 * @param  y - координата на сетке
	 * @return
	 */
	Vector3 interp(double x, double y);

	/**
	 * Пересчитывает сеточные координаты
	 */
	void reshape();

	/**
	 * Обнуляет массив с узлами
	 */
	void clear();

	// Get свойства
	int get_nx() { return nx; }
	int get_ny() { return ny; }
	double get_ax() { return ax; }
	double get_bx() { return bx; }
	double get_ay() { return ay; }
	double get_by() { return by; }
	double get_dx() { return dx; }
	double get_dy() { return dy; }
	double get_kx() { return kx; }
	double get_ky() { return ky; }

	// Set свойства
	void set_dx(double value) {
		this->dx = (value <= VM2_MIN_DIFF ? VM2_MIN_DIFF : value);
	}

	void set_dy(double value) {
		this->dy = (value <= VM2_MIN_DIFF ? VM2_MIN_DIFF : value);
	}

	void set_xrange(double a, double b) {
    if (a < b) { this->ax = a; this->bx = b; }
    else       { this->ax = b; this->bx = a; }
  }

  void set_yrange(double a, double b) {
    if (a < b) { this->ay = a; this->by = b; }
    else       { this->ay = b; this->by = a; }
  }

private:
	// Число узлов
	int nx, ny;
	// Границы сетки
	double ax, bx, ay, by;
	// Шаги сетки
	double dx, dy;
	// Коэффициенты пересчета из реальных координат в сеточные
	double kx, ky;
	// Массив с векторами в узлах сетки
	Vector3 * data;
};

#endif
