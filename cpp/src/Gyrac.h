#ifndef SRC_GYRAC_H
#define SRC_GYRAC_H

#include <vector>
#include <cmath>
#include <cstdlib>
//#include <iostream>
#include "Particle.h"
#include "VectorMesh2.h"
#include "EFieldMesh3.h"
#include "GyracConfig.h"

// Константы -------------------------------------------------------------------
#define PI 3.14159265358979323846
#define CC 3.0e10	//Скорость света в см/c
#define EZ 4.8e-10	//заряд электрона в СГС
#define ME 9.1e-28	//масса электрона в граммах
#define MP 1.7e-24  //масса протона в граммах
#define HF 2.45e09	//частота резонатора

// Вспомогательные функции -----------------------------------------------------

/**
 * Рандомизатор от нуля до одного
 * @return
 */
double math_random();

/**
 * Функция производит инжекцию цилиндра в вектор с частицами
 *
 * @param pos       координата центра цилиндра
 * @param rm        радиус цилиндра
 * @param lm        длина цилиндра
 * @param umin      минимальная энергия инжектируемх частиц
 * @param umax      максимальная энергия инжектируемы частиц
 * @param n         число инжектируемых частиц
 * @param container вектор с частицами
 */
void inject_cylinder(
	Vector3 *pos,
	double rm, double lm,
	double umin, double umax,
	int n, std::vector<Particle> *container
);

/**
 * Функция производит инжекцию шара в вектор с частицами
 * 
 * @param pos       координата центра шара
 * @param rm        радиус шара
 * @param umin      минимальная энергия инжектируемх частиц
 * @param umax      максимальная энергия инжектируемы частиц
 * @param n         число инжектируемых частиц
 * @param container вектор с частицами
 */
void inject_sphere(
	Vector3 *pos, double rm,
	double umin, double umax,
	int n, std::vector<Particle> *container
);

// Gyrac class -----------------------------------------------------------------

/**
 * Класс для моделирования установки GYRAC
 */
class Gyrac
{
private:
	//Переменные
	double
		//циклотронная частота электрона
		om,
		//релятивистский радиус ицклотронного вращения
		rl,
		//Модельная длина резонатора
		Lm,
		//Модельный радиус резонатора
		Rm,
		//Напряженность СВЧ поля в СГС
		E_hf,
		//Магнитное поле для электрона при ЭЦР для электрона
		B0e,
		//Обратное магнитное поле для электрона
		B0e_inv,
		//Магнитное поле для электрона при ЭЦР для иона
		B0i,
		//Обратное магнитное поле для иона
		B0i_inv,
		//Нормированный временной шаг
		dt;
	int
		//Чисто протонов в ионе
		pN,
		//Число нейтронов в ионе
		nN,
		//Число шагов за период
		steps_for_period;

	//Число шагов вычисления
	long step_counter;
	//Шаги СВЧ поля за один период
	std::vector<Vector3> hf_steps;
	//Фаза электрического свч поля
	int hf_phase;

	//список с электронами
	std::vector<Particle> electrons;
	//Список с ионами
	std::vector<Particle> ions;
	//Электроны за пределами ловушки
	std::vector<Particle> losses;

	//число шагов до следующей инжекции
	int ei_interval;
	//число инжектируемых электронов
	int ei_count;
	//Обратный отсчет до новой инжекции
	int ei_countdown;

	//Параметры инжектируемого цилиндра электронов
	double ei_umin;
	double ei_umax;
	double ei_Rm;
	double ei_Lm;

	//Заряд модельного электрона
	double qme;

	//Сетка для вычисления магнитного поля
	VectorMesh2* mesh_B;
	EFieldMesh3* mesh_E;

	//СВЧ поля
	void hf_init();

	//Инициализация
	void init(GyracConfig &config);

public:
	Gyrac(GyracConfig &config);
	~Gyrac(void);

	//Флаги ----------------------------------------------------------------------
	bool use_homogeneous_E;
	bool use_homogeneous_B;
	bool use_hf;
	bool use_mesh_B;
	bool use_sc;
	bool use_ei;
	bool use_bounds;
	bool use_keep_losses;
	bool use_own_field;

	//Отключение всех флагов
	void disable_all()
	{
		use_homogeneous_E = false;
		use_homogeneous_B = false;
		use_hf = false;
		use_sc = false;
		use_mesh_B = false;
		use_ei = false;
		use_bounds = false;
		use_keep_losses = false;
		use_own_field = false;
	}

	//Методы конвертации ---------------------------------------------------------
	double conv_x_xm(double x) { return x / rl; }
	double conv_xm_x(double xm) { return xm * rl; }

	double conv_ev_erg(double ev)
	{
		return ev * 1.602176E-12;
	}

	double conv_erg_ev(double erg)
	{
		return erg / 1.602176E-12;
	}

	double conv_ev_u(double ev, double m0)
	{
		double gamma = conv_ev_erg(ev) / (m0 * CC * CC) + 1;
		return sqrt(gamma * gamma - 1);
	}

	double conv_u_ev(double u, double m0)
	{
		double gamma = sqrt(u * u + 1);
		return conv_erg_ev(m0 * CC * CC * (gamma - 1));
	}

	double conv_steps_t(int steps)
	{
		return steps / (HF * steps_for_period);
	}

	// Инжекция ------------------------------------------------------------------

	void ei_init(double r, double L, double ev_min, double ev_max, double ne);

	void ei_update();

	//Электрическое и магнитное поля ---------------------------------------------

	//однородное в пространстве электрическое электрическое и магнитные поля
	Vector3 homogeneous_E;
	Vector3 homogeneous_B;

	//Поле СВЧ
	Vector3 field_E_hf(Vector3&);

	//Суперпозиция электрического поля
	Vector3 field_E_total(Vector3& pos);

	//Функция для обновления параметров электрических полей
	void E_update();

	//Магнитное поле с сетки
	Vector3 field_B_mesh(Vector3&);

	//Суперпозиция магнитного поля
	Vector3 field_B_total(Vector3& pos);

	//Методы движения частиц -----------------------------------------------------
	void move_electrons();

	void move_ions();

	//Методы анализа местоположения частиц
	bool in_bounds(Vector3& pos);

	//Методы вычисления ----------------------------------------------------------
	void step();
	void evalute(long steps);
	void reset();

	//Get/Set
	double get_om() { return om; }
	double get_rl() { return rl; }
	double get_Lm() { return Lm; }
	double get_Rm() { return Rm; }
	double get_B0e() { return B0e; }
	double get_B0e_inv() { return B0e_inv; }
	double get_B0i() { return B0i; }
	double get_B0i_inv() { return B0i_inv; }
	double get_dt() { return dt; }
	double get_steps_for_period() { return steps_for_period; }
	int get_pN() { return pN; }
	int get_nN() { return nN; }
	long get_step_counter() { return step_counter; }
	double get_E_hf() { return E_hf; }
	void set_E_hf(double value) { E_hf = value; }
	std::vector<Particle> * get_electrons() { return &electrons; }
	std::vector<Particle> * get_ions() { return &ions; }
	std::vector<Particle> * get_losses() { return &losses; }

	int get_ei_interval() { return ei_interval; }
	void set_ei_interval(int value) { ei_interval = value; }

	int get_ei_count() { return ei_count; }
	void set_ei_count(int value) { ei_count = value; }

	double get_ei_umin() { return ei_umin; }
	void set_ei_umin(double value) { ei_umin = value; }

	double get_ei_umax() { return ei_umax; }
	void set_ei_umax(double value) { ei_umax = value; }

	double get_ei_Rm() { return ei_Rm; }
	void set_ei_Rm(double value) { ei_Rm = value; }

	double get_ei_Lm() { return ei_Lm; }
	void set_ei_Lm(double value) { ei_Lm = value; }

	double get_qme() { return qme; }
	void set_qme(double value) { qme = value; }

	VectorMesh2* get_mesh_B() { return mesh_B; }

	EFieldMesh3* get_mesh_E() { return mesh_E; }
};

#endif
