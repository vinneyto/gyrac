
#ifndef SRC_VECTOR3_H_
#define SRC_VECTOR3_H_

#include <math.h>


class Vector3
{
    public:

        double   x;
        double   y;
        double   z;

		Vector3()
		{
			x = 0.0;
			y = 0.0;
			z = 0.0;
		}

        Vector3(double r, double s, double t)
        {
            x = r;
            y = s;
            z = t;
        }

        Vector3& Set(double r, double s, double t)
        {
            x = r;
            y = s;
            z = t;
            return (*this);
        }

        double& operator [](long k)
        {
            return ((&x)[k]);
        }

        const double& operator [](long k) const
        {
            return ((&x)[k]);
        }

        Vector3& operator +=(const Vector3& v)
        {
            x += v.x;
            y += v.y;
            z += v.z;
            return (*this);
        }

        Vector3& operator -=(const Vector3& v)
        {
            x -= v.x;
            y -= v.y;
            z -= v.z;
            return (*this);
        }

        Vector3& operator *=(double t)
        {
            x *= t;
            y *= t;
            z *= t;
            return (*this);
        }

        Vector3& operator /=(double t)
        {
            double f = 1.0F / t;
            x *= f;
            y *= f;
            z *= f;
            return (*this);
        }

        Vector3& operator %=(const Vector3& v)
        {
            double       r, s;

            r = y * v.z - z * v.y;
            s = z * v.x - x * v.z;
            z = x * v.y - y * v.x;
            x = r;
            y = s;

            return (*this);
        }

        Vector3& operator &=(const Vector3& v)
        {
            x *= v.x;
            y *= v.y;
            z *= v.z;
            return (*this);
        }

        Vector3 operator -(void) const
        {
            return (Vector3(-x, -y, -z));
        }

        Vector3 operator +(const Vector3& v) const
        {
            return (Vector3(x + v.x, y + v.y, z + v.z));
        }

        Vector3 operator -(const Vector3& v) const
        {
            return (Vector3(x - v.x, y - v.y, z - v.z));
        }

        Vector3 operator *(double t) const
        {
            return (Vector3(x * t, y * t, z * t));
        }

        Vector3 operator /(double t) const
        {
            double f = 1.0F / t;
            return (Vector3(x * f, y * f, z * f));
        }

        double operator *(const Vector3& v) const
        {
            return (x * v.x + y * v.y + z * v.z);
        }

        Vector3 operator %(const Vector3& v) const
        {
            return (Vector3(y * v.z - z * v.y, z * v.x - x * v.z,
                    x * v.y - y * v.x));
        }

        Vector3 operator &(const Vector3& v) const
        {
            return (Vector3(x * v.x, y * v.y, z * v.z));
        }

        bool operator ==(const Vector3& v) const
        {
            return ((x == v.x) && (y == v.y) && (z == v.z));
        }

        bool operator !=(const Vector3& v) const
        {
            return ((x != v.x) || (y != v.y) || (z != v.z));
        }

        Vector3& Normalize(void)
        {
            return (*this /= sqrt(x * x + y * y + z * z));
        }

        Vector3& RotateAboutX(double angle);
        Vector3& RotateAboutY(double angle);
        Vector3& RotateAboutZ(double angle);
        Vector3& RotateAboutAxis(double angle, const Vector3& axis);

		double SquaredMag() { return x*x + y*y + z*z; }
		double Magnitude() { return sqrt(SquaredMag()); }
};


inline Vector3 operator *(double t, const Vector3& v)
{
    return (Vector3(t * v.x, t * v.y, t * v.z));
}

inline double Dot(const Vector3& v1, const Vector3& v2)
{
    return (v1 * v2);
}

inline Vector3 Cross(const Vector3& v1, const Vector3& v2)
{
    return (v1 % v2);
}

inline double Magnitude(const Vector3& v)
{
    return (sqrt(v.x * v.x + v.y * v.y + v.z * v.z));
}

inline double InverseMag(const Vector3& v)
{
    return (1.0F / sqrt(v.x * v.x + v.y * v.y + v.z * v.z));
}

inline double SquaredMag(const Vector3& v)
{
    return (v.x * v.x + v.y * v.y + v.z * v.z);
}

#endif /* SRC_VECTOR3_H_ */
