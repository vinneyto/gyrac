#ifndef SRC_UTILS_H
#define SRC_UTILS_H

/**
 * Возвращает true если value больше или равно a и меньше b
 * @param  value
 * @param  a
 * @param  b
 * @return
 */
bool inRange(double value, double a, double b);

/**
 * Возвращает true если value больше или равно a и меньше b
 * @param  value
 * @param  a
 * @param  b
 * @return
 */
bool inRange(int value, int a, int b);

/**
 * Ограничивает value диапазоном между a (включительно) и b (включительно)
 * @param  value
 * @param  a
 * @param  b
 * @return
 */
int truncate(int value, int a, int b);

#endif
