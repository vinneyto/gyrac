#include "utils.h"

bool inRange(double value, double a, double b)
{
  return (value >= a && value < b);
}

bool inRange(int value, int a, int b)
{
  return (value >= a && value < b);
}

int truncate(int value, int a, int b)
{
  if (value < a) value = a;
  if (value > b) value = b;
  return value;
}
