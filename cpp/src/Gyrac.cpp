#include "Gyrac.h"
#include <iostream>

using namespace std;

// Вспомогательные функции -----------------------------------------------------

double math_random() {
	return (double)(rand())/(RAND_MAX + 1.0);
}

void inject_cylinder(Vector3 *pos, double lm, double rm, double umin, double umax, int n, std::vector<Particle> *container)
{
	for (int i = 0; i < n; i++)
	{
		double z = lm * math_random() - lm / 2.0;
		double r = rm * math_random();
		double phi = 2 * PI * math_random();
		double u = umin + (umax - umin) * math_random();

		Particle e;

		e.position.x = pos->x + r * cos(phi);
		e.position.y = pos->y + r * sin(phi);
		e.position.z = pos->z + z;

		e.impulse.x = 0;
		e.impulse.y = 0;
		e.impulse.z = u;

		container->push_back(e);
	}
}

void inject_sphere(Vector3 *pos, double rm, double umin, double umax, int n, std::vector<Particle> *container)
{
	for (int i = 0; i < n; i++)
	{
		double r = rm * math_random();
		double phi = 2.0 * PI * math_random() * 1;
		double theta = acos(2.0 * math_random() - 1);
		double u = umin + (umax - umin) * math_random();

		Particle e;

		e.position.x = pos->x + r * sin(theta) * cos(phi);
		e.position.y = pos->y + r * sin(theta) * sin(phi);
		e.position.z = pos->z + r * cos(theta);

		e.impulse.x = 0;
		e.impulse.y = 0;
		e.impulse.z = u;

		container->push_back(e);
	}
}

// Gyrac class -----------------------------------------------------------------

Gyrac::Gyrac(GyracConfig &config)
{
	init(config);
}

void Gyrac::init(GyracConfig &config)
{
	steps_for_period = config.steps_for_period;
	//Число протонов в ионе
	pN = config.pN;
	//Число нейтронов в ионе
	nN = config.nN;
	//циклотронная частота
	om = 2.0 * PI * HF;
	//релятивистский радиус циклотронного вращения
	rl = CC / om;
	//длина и радиус резонатора в модельных координатах
	Lm = conv_x_xm(config.L);
	Rm = conv_x_xm(config.R);
	//Магнитное поле при ЭЦР для электрона
	B0e = (ME * CC * om) / EZ;
	//Обратное магнитное поле (для нормировки)
	B0e_inv = 1 / B0e;
	//Магнитное поле при ЭЦР для иона
	B0i = ((nN + pN) * MP * CC * om) / (pN * EZ);
	//Обратное магнитное поле (для нормировки)
	B0i_inv = 1 / B0i;
	//Амплитуда свч поля
	E_hf =  config.E_hf / 300.0; //конвертируем электрическое поле в гаусы
	//нормированный временной шаг
	dt = 2.0 * PI / steps_for_period;
	//Модельный заряд частицы
	qme = config.qm_ratio * EZ;
	//счетчик шагов
	step_counter = 0;
	//Инициализация сетки магнитного поля
	//Сетка инициализируется в модельных координатах!!!
	mesh_B = new VectorMesh2(
		conv_x_xm(config.mesh_B_dz), //dx
		conv_x_xm(config.mesh_B_dr), //dy
		-Lm / 2.0, Lm / 2.0,		 //ax, bx
		0.0, Rm						 //ay, by
		);
	//Инициализация сетки электрического поля
	mesh_E = new EFieldMesh3(
		conv_x_xm(config.mesh_E_dx),
		conv_x_xm(config.mesh_E_dy),
		conv_x_xm(config.mesh_E_dz),
		-Rm, Rm,
		-Rm, Rm,
		-Lm/2.0, Lm/2.0
		);
	//Инициализация СВЧ поля
	hf_init();
	//Инициализируем инжекцию
	//ei_init(0.1, 0.5, 50.0, 100.0, 1e8);
	//Флажки
	disable_all();
	//Сбрасываем счетчики
	reset();
}

Gyrac::~Gyrac(void)
{
	delete mesh_B;
	delete mesh_E;
}

//Итерация основного вычислительного цикла -------------------------------------

void Gyrac::step()
{
	//Обновляем электрические поля
	E_update();

	//Инжектируем электроны если нужно
	if (use_ei)
		ei_update();

	//Двигаем электроны
	move_electrons();

	//Двигаем ионы
	move_ions();

	//Увеличиваем счетчик шагов
	step_counter++;
}

void Gyrac::evalute(long steps)
{
	for (long i = 0; i < steps; i++) { step(); }
}

void Gyrac::reset()
{
	electrons.clear();
	ions.clear();
	losses.clear();
	step_counter = 0;
	ei_countdown = 0;
	hf_phase = 0;
}

//Электрические и магнитные поля -----------------------------------------------

//Инициализация СВЧ поля
void Gyrac::hf_init()
{
	int N = steps_for_period;

	for (int i = 0; i < N; i++)
	{
		Vector3 v;
		v.x = cos(2.0 * PI * i / N);
		v.y = sin(2.0 * PI * i / N);
		hf_steps.push_back(v);
	}

	//Начальная фаза для СВЧ поля
	hf_phase = 0;
}

void Gyrac::E_update()
{
	//Свч поля
	if (use_hf)
	{
		hf_phase++;
		if (hf_phase >= steps_for_period)
			hf_phase = 0;
	}

	//Вычисление сеточного потенциала
	if (use_sc)
	{
		//Чистим сетку
		mesh_E->clear();

		//Добавляем на сетку заряд электронов
		mesh_E->append_q(electrons, -4.0 * PI * -1.0 * qme / mesh_E->get_cell_V());

		//Добавляем на сетку заряд ионов
		mesh_E->append_q(ions, -4.0 * PI * qme * pN / mesh_E->get_cell_V());

		//Вычисляем потенциал и электр поле
		mesh_E->poisson();
	}
}

// Методы возвращающие элекрическое и магнитное поля в точке -------------------

Vector3 Gyrac::field_E_hf(Vector3& pos)
{
	Vector3 field = hf_steps[hf_phase];

	Vector3 res;
	res.x = E_hf * field.x * (pos.x * pos.y)/4 * cos(PI * pos.z/Lm);
	res.y = E_hf * field.y * cos(PI * pos.z/Lm);
	res.z = 0;

	return res;
}

Vector3 Gyrac::field_B_mesh(Vector3& pos)
{
	//Вектор перпендикулярный оси Z
	Vector3 mesh_r = Vector3(pos.x, pos.y, 0);
	//Для сетки x=z и y=длина вектора mesh_r
	double mesh_x = pos.z,
		   mesh_y = mesh_r.Magnitude();

	//Получаем трехмерный вектор который содержит только x, y компоненты
	Vector3 B2d = mesh_B->interp(mesh_x, mesh_y);

	//R компонента магнитного поля направлена вдоль вектора vecr
	//нормализуем vecr и умножаем его на r компоненту магнитного поля
	Vector3 B3d;
	if (mesh_y == 0.0)
		//Если точка находится на оси R компонента магнитного поля равна нулю
		B3d = Vector3(0.0, 0.0, 0.0);
	else
		//Если не на оси - нормируем x, y радиус вектор
		B3d = mesh_r.Normalize() * B2d.y;
	//Добавляем к магнитному полю компоненту Z
	B3d.z = B2d.x;

	return B3d;
}

//Суперпозиция электрического поля
Vector3 Gyrac::field_E_total(Vector3& pos)
{
	Vector3 E(0.0, 0.0, 0.0);
	//Однородное поле
	if (use_homogeneous_E)
		E += homogeneous_E;

	//СВЧ поле
	if (use_hf)
		E += field_E_hf(pos);

	//Сеточное поле
	if (use_sc)
		E += mesh_E->interp_E(pos);

	return E;
}

//Суперпозиция магнитного поля
Vector3 Gyrac::field_B_total(Vector3& pos)
{
	Vector3 B(0.0, 0.0, 0.0);
	//Однородное поле
	if (use_homogeneous_B)
		B += homogeneous_B;

	//Сеточное поле
	if (use_mesh_B)
		B += field_B_mesh(pos);

	return B;
}

// Инжекция --------------------------------------------------------------------

void Gyrac::ei_init(double L, double r, double ev_min, double ev_max, double ne)
{
	ei_Lm = conv_x_xm(L);
	ei_Rm = conv_x_xm(r);
	ei_umin = conv_ev_u(ev_min, ME);
	ei_umax = conv_ev_u(ev_max, ME);

	double u = ei_umin + (ei_umax - ei_umin) / 2;
	ei_interval = (int) (ei_Lm / (u * dt));

	double Q = PI * r*r * L * ne * EZ;
	ei_count = (int) (Q / qme);

	ei_countdown = 0;
}

void Gyrac::ei_update()
{

	Vector3 inj_point = Vector3(0.0, 0.0, -Lm / 2.0 + ei_Lm / 2.0);

	//Инжекция электронов
	if (ei_countdown <= 0)
	{
		inject_cylinder(
		&inj_point,
		ei_Lm, ei_Rm,
		ei_umin, ei_umax,
		ei_count,
		&electrons
		);

		ei_countdown = ei_interval;
	}
	ei_countdown--;
}

//Решение уравнений движения ---------------------------------------------------

void Gyrac::move_electrons()
{
	double hdt = dt/2;

	std::vector<Particle>::iterator iter = electrons.begin();

	while (iter != electrons.end())
	{
		Particle p = *iter;

		//Получаем нормированные поля
		Vector3 E = -B0e_inv * field_E_total(p.position);
		Vector3 B = -B0e_inv * field_B_total(p.position);

		//1. прибавление половины импульса электрических сил к импульсу частицы в момент времени n-1/2
		p.impulse += E * hdt;

		//2. вращение вектора импульса заряженной частицы в магнитном поле
		double gamma = sqrt(p.impulse.SquaredMag() + 1);

		Vector3 t = B * hdt / gamma;
		Vector3 s = 2 * t / (1 + t.SquaredMag());

		Vector3 us = p.impulse + Cross(p.impulse, t);
		p.impulse += Cross(us, s);

		//3. прибавление второй половины импульса электрических сил.
		p.impulse += E * hdt;

		//4. расчет новых координат частицы, нормированных на релятивистский радиус циклотронного вращения электрона
		gamma = sqrt(p.impulse.SquaredMag() + 1);

		p.position += p.impulse * dt / gamma;

		*iter = p;

		//5. удаляем частицы за пределами ловушки
		if (use_bounds)
		{
			if (!in_bounds(p.position))
			{
				if (use_keep_losses) losses.push_back(p);

				electrons.erase(iter);
			}
			else { ++iter; }
		} else { ++iter; }

	}
}

void Gyrac::move_ions()
{
	//Ионы двигаются только посредством собственного поля плазмы
	if (use_sc)
	{
		std::vector<Particle>::iterator iter = ions.begin();

		while (iter != ions.end())
		{
			Particle p = *iter;

			//Ионы двигаются только за счет собственного поля плазмы
			Vector3 E = B0i_inv * mesh_E->interp_E(p.position);

			//Изменяем импульс частицы
			p.impulse += E * dt;

			//Изменяем координаты частицы
			//double gamma = sqrt(p.impulse.SquaredMag() + 1);

			//p.position += p.impulse * dt / gamma;

			p.position += p.impulse * dt;

			*iter = p;

			if (use_bounds)
			{
				if (!in_bounds(p.position))
				{
					ions.erase(iter);
				}
				else { ++iter; }
			}
			else { ++iter; }
		}
	}
}

bool Gyrac::in_bounds(Vector3& pos)
{
	double hlm = Lm / 2.0;
	if (pos.z > hlm || pos.z < -hlm) return false;
	if (sqrt(pos.x*pos.x + pos.y*pos.y) > Rm) return false;
	return true;
}

/*
void Gyrac::inject_particle(double z, double r, double phi, double v_phi, double v_theta, double ev, PType type)
{
	Particle p;
	p.position.x = conv_x_xm(r * cos(phi));
	p.position.y = conv_x_xm(r * sin(phi));
	p.position.z = conv_x_xm(z);

	double sin_v_phi = sin(v_phi);
	p.impulse.x = sin_v_phi * cos(v_theta);
	p.impulse.y = sin_v_phi * sin(v_theta);
	p.impulse.z = cos(v_phi);

	switch (type)
	{
	case Gyrac::ELECTRON:
		//Инжектируем электрон
		p.impulse *= conv_ev_u(ev, ME);
		electrons.push_back(p);
		break;
	case Gyrac::ION:
		//Инжектируем ион
		break;
	default:
		break;
	}
}
*/
