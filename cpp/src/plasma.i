%module plasma
%{
	#include "Vector3.h"
	#include "Particle.h"
	#include "fishpack.h"
	#include "VectorMesh2.h"
	#include "EFieldMesh3.h"
	#include "GyracConfig.h"
	#include "Gyrac.h"
	#include <cstdlib>
	#include <ctime>
%}

%init %{
	//Обновление рандомизации
	srand(time(NULL));
%}

%include "std_vector.i"

namespace std {
   %template(ParticleVector) vector<Particle>;
}

%include "Vector3.h"
%include "Particle.h"
%include "fishpack.h"
%include "VectorMesh2.h"
%include "EFieldMesh3.h"
%include "GyracConfig.h"
%include "Gyrac.h"
