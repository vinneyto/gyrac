#ifndef SRC_FISHPACK_H
#define SRC_FISHPACK_H

extern "C" {
	// void vntfill_(int*, int *, int *);

	/**
	 * Вычисляет уравнение Гельмгольца на трехмерной сетке
	 *
	 * (D/DX)(DU/DX) + (D/DY)(DU/DY) + (D/DZ)(DU/DZ) + LAMBDA*U = F(X,Y,Z)
	 *
	 * псевдодвухмерный массив (ni,nj):
	 * одномерный массив длины ni*nj
	 * доступ к элементу a(i,j) == a[i + j*ni]
	 *
	 * псевдотрехмерный массив (ni,nj,nk):
	 * одномерный массив длины ni*nj*nk
	 * доступ к элементу a(i,j,k) == a[i + j*ni + k*ni*nj]
	 *
	 * подробне смотри в hw3crt.f
	 *
	 * @param XS     левая граница сетки по X
	 * @param XF     правая граница сетки по X
	 * @param L      число интервалов сетки по X (число узлов = L+1)
	 * @param LBDCND вид граничных условия по X
	 * @param BDXS   псевдодвухмерный массив со значениями производной по X где X = XS. длина (M+1)*(N+1)
	 * @param BDXF   псевдодвухмерный массив со значениями производной по X где X = XF. длина (M+1)*(N+1)
	 *
	 * @param YS     левая граница сетки по Y
	 * @param YF     правая граница сетки по Y
	 * @param M      число интервалов сетки по Y (число узлов = M+1)
	 * @param MBDCND вид граничных условия по Y
	 * @param BDYS   псевдодвухмерный массив со значениями производной по Y где Y = YS. длина (L+1)*(N+1)
	 * @param BDYF   псевдодвухмерный массив со значениями производной по Y где Y = YF. длина (L+1)*(N+1)
	 *
	 * @param ZS     левая граница сетки по Z
	 * @param ZF     правая граница сетки по Z
	 * @param N      число интервалов сетки по Z (число узлов = N+1)
	 * @param NBDCND вид граничных условия по Z
	 * @param BDZS   псевдодвухмерный массив со значениями производной по Z где Z = ZS. длина (L+1)*(M+1)
	 * @param BDZF   псевдодвухмерный массив со значениями производной по Z где Z = ZF. длина (L+1)*(M+1)
	 * @param ELMBDA константа LAMBDA. для уравнения Пуассона равна нулю!
	 * @param LDIMF  число. должно быть больше равно L+1
	 * @param MDIMF  число. должно быть больше равно M+1
	 * @param F      псевдотрехмерный массив длины (L+1)*(M+1)*(N+1)
	 *               на входе в массиве могут содержаться граничные условия
	 *               на выходе решение уравнения Гельмгольца
	 * @param PERTRB вычислительная ошибка
	 * @param IERROR код ошибки
	 * @param W      одномерный массив длины 30 + L + M + 5*N + MAX(L,M,N) + 7*(INT((L+1)/2) + INT((M+1)/2))
	 */
	void hw3crt_(
		float *XS, float *XF, int *L, int *LBDCND, float *BDXS, float *BDXF,
		float *YS, float *YF, int *M, int *MBDCND, float *BDYS, float *BDYF,
		float *ZS, float *ZF, int *N, int *NBDCND, float *BDZS, float *BDZF,
		float *ELMBDA, int *LDIMF, int *MDIMF, float *F,
		float *PERTRB, int *IERROR, float *W
	);
}

#endif
