#include "EFieldMesh3.h"
#include <iostream>
#include "fishpack.h"

using namespace std;

// Реализация сетки электрического поля
EFieldMesh3::EFieldMesh3(double dx, double dy, double dz,
                			   double ax, double bx,
                			   double ay, double by,
                			   double az, double bz)
{
  this->set_dx(dx);
	this->set_dy(dy);
	this->set_dz(dz);

	this->set_xrange(ax, bx);
  this->set_yrange(ay, by);
  this->set_zrange(az, bz);

	//Обнуляем указатель
	qphi = NULL;
	efld = NULL;
  w = NULL;

	reshape();
}


EFieldMesh3::~EFieldMesh3(void)
{
	delete[] qphi;
  delete[] efld;
  delete[] w;
}

void EFieldMesh3::reshape()
{
	if (qphi != NULL) {
		delete[] qphi;
		delete[] efld;
	}

	kx = 1. / dx;
	ky = 1. / dy;
	kz = 1. / dz;

	//Вычисляем число сеточных узлов
	//+1.5 для округления в большую сторону
	nx = (bx - ax) * kx + 1.5;
	ny = (by - ay) * ky + 1.5;
	nz = (bz - az) * kz + 1.5;

	//Вычисляем чисто сеточных интервалов
	nix = nx - 1;
	niy = ny - 1;
	niz = nz - 1;

	//Зарядно-потенциальная сетка
	qphi = new float[nx*ny*nz];
	//Сетка с электрическим полем
	efld = new Vector3[nx*ny*nz];
  //Вспомогательный массив
  w = new float[calc_wsize()];

	cell_V = dx * dy * dz;

	clear();
}

//Добавляет заряд на зарядно-потенциальную сетку
void EFieldMesh3::append_q(const Vector3& v, double value)
{
	//Перевод в сеточные координаты
	double x = (v.x - ax) * kx;
  double y = (v.y - ay) * ky;
  double z = (v.z - az) * kz;

	//Поиск индексов ячейки
	int xi = (int)x;
	int yi = (int)y;
	int zi = (int)z;

  // Если индекс за пределами массива (с учетом добавки единицы) - прерываем выполнение
  if (!inRange(xi, 0, nix) ||
      !inRange(yi, 0, niy) ||
      !inRange(zi, 0, niz)) return;

	for (int k = 0; k < 2; k++)
		for (int j = 0; j < 2; j++)
			for (int i = 0; i < 2; i++)
				qphi[getIndex(xi + i, yi + j, zi + k)] +=
					(float)(abs(xi+1-i-x) * abs(yi+1-j-y) * abs(zi+1-k-z) * value);

	// qphi[getIndex(xi, yi, zi)] += (xi + 1 - x) * (yi + 1 - y) * (zi + 1 - z) * value;
	// qphi[getIndex(xi + 1, yi, zi)] += (x - xi) * (yi + 1 - y) * (zi + 1 - z) * value;
	// qphi[getIndex(xi, yi + 1, zi)] += (xi + 1 - x) * (y - yi) * (zi + 1 - z) * value;
	// qphi[getIndex(xi + 1, yi + 1, zi)] += (x - xi) * (y - yi) * (zi + 1 - z) * value;
  //
	// qphi[getIndex(xi, yi, zi + 1)] += (xi + 1 - x) * (yi + 1 - y) * (z - zi) * value;
	// qphi[getIndex(xi + 1, yi, zi + 1)] += (x - xi) * (yi + 1 - y) * (z - zi) * value;
	// qphi[getIndex(xi, yi + 1, zi + 1)] += (xi + 1 - x) * (y - yi) * (z - zi) * value;
	// qphi[getIndex(xi + 1, yi + 1, zi + 1)] += (x - xi) * (y - yi) * (z - zi) * value;

}

void EFieldMesh3::append_q(std::vector<Particle> &particles, double value)
{

	//Собираем заряд на сетку
	std::vector<Particle>::iterator iter = particles.begin();

	//Добавляем заряд всех электронов на сетку
	while (iter != particles.end())
	{
		Particle p = *iter;

		//Добавляем на сетку модельный заряд каждого электрона (модельный заряд также измеряется в системе СГС, просто он гораздо больше реального)
		append_q(p.position, value);

		++iter;
	}
}

//Возвращает вектор электрического поля в точке
Vector3 EFieldMesh3::interp_E(const Vector3 &v)
{
	double x = (v.x - ax) * kx;
  double y = (v.y - ay) * ky;
  double z = (v.z - az) * kz;

	int xi = (int)x;
	int yi = (int)y;
	int zi = (int)z;

  if (!inRange(xi, 0, nix) ||
      !inRange(yi, 0, niy) ||
      !inRange(zi, 0, niz)) return Vector3(0., 0., 0.);

	Vector3 res(0, 0, 0);

	for (int k = 0; k < 2; k++)
		for (int j = 0; j < 2; j++)
			for (int i = 0; i < 2; i++)
				res += efld[getIndex(xi + i, yi + j, zi + k)] *
					abs(xi+1-i-x) * abs(yi+1-j-y) * abs(zi+1-k-z);

	return res;
}

void EFieldMesh3::poisson()
{
  // void hw3crt_(
  //   double *XS, double *XF, int *L, int *LBDCND, double *BDXS, double *BDXF,
  //   double *YS, double *YF, int *M, int *MBDCND, double *BDYS, double *BDYF,
  //   double *ZS, double *ZF, int *N, int *NBDCND, double *BDZS, double *BDZF,
  //   double *ELMBDA, int *LDIMF, int *MDIMF, double *F,
  //   double *PERTRB, int *IERROR, double *W
  // );

  int periodic = 0;
  int dirichlet = 1;
  float lambda = 0.0;

  // Преобразование типов для передачи в фортран
  float fax = (float) ax;
  float fbx = (float) bx;
  float fay = (float) ay;
  float fby = (float) by;
  float faz = (float) az;
  float fbz = (float) bz;

  // Заполняем граничные условия по оси Z нулями
  // Граничные условия по оси Z - по осям X,Y
  for (int j = 0; j < ny; j++) {
    for (int i = 0; i < nx; i++) {
      qphi[getIndex(i, j, 0)]   = 0.0;
      qphi[getIndex(i, j, niz)] = 0.0;
    }
  }

  // Производим вычисление потенциала
  hw3crt_(
    &fax, &fbx, &nix, &periodic, NULL, NULL,
    &fay, &fby, &niy, &periodic, NULL, NULL,
    &faz, &fbz, &niz, &dirichlet, NULL, NULL,
    &lambda, &nx, &ny, qphi,
    &computing_error, &error, w
  );

  // Если есть ошибка - выходим
  if (error != 0) return;

	//Вычисляем электрическое поле потенциала
	for (int k = 1; k < niz; k++) {
		for (int j = 1; j < niy; j++) {
			for (int i = 1; i < nix; i++) {
				Vector3 E(0, 0, 0);

				E.x = (qphi[getIndex(i - 1, j, k)] -
					     qphi[getIndex(i + 1, j, k)]) / (2 * dx);

				E.y = (qphi[getIndex(i, j - 1, k)] -
					     qphi[getIndex(i, j + 1, k)]) / (2 * dy);

				E.z = (qphi[getIndex(i, j, k - 1)] -
					     qphi[getIndex(i, j, k + 1)]) / (2 * dz);

				efld[getIndex(i, j, k)] = E;
			}
		}
	}
}

float EFieldMesh3::get_qphi_total() {
  float total = 0.0;
  int size = nx*ny*nz;
  for (int i = 0; i < size; i++)
	{
		total += qphi[i];
	}
  return total;
}

//Чистит сетки
void EFieldMesh3::clear()
{
	int size = nx*ny*nz;
	for (int i = 0; i < size; i++)
	{
		qphi[i] = 0.0;
		efld[i] = Vector3(0, 0, 0);
	}
}
