#ifndef SRC_GYRAC_CONFIG_H
#define SRC_GYRAC_CONFIG_H

class GyracConfig
{
public:

	double L, R, E_hf, qm_ratio;
	int pN, nN, steps_for_period;
	double mesh_B_dz, mesh_B_dr, mesh_E_dx, mesh_E_dy, mesh_E_dz;

	GyracConfig()
	{
		L = 8.0; //длина резонатора
		R = 6.0; //радиус резонатора
		E_hf = 500.0; //500 вольт на см
		qm_ratio = 1e9 / 1e5; //отношение модельного заряда к реальном
		//По умолчанию ионы - альфачастицы
		pN = 2; //Двузарядные ионы
		nN = 2; //Двунейтронные ионы
		steps_for_period = 250; //Число шагов СВЧ поля за период
		//дефолтный шаг всех сеток равен 0.2
		mesh_B_dz = mesh_B_dr = 0.2;
		mesh_E_dx = mesh_E_dy = mesh_E_dz = 0.2;
	};
	~GyracConfig() {};
};

#endif
