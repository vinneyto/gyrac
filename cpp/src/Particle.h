#ifndef PARTICLE_H_
#define PARTICLE_H_

#include "Vector3.h"

/**
 * Структура для определения частицы
 */
struct Particle {
	Vector3 position; // Координата частицы
	Vector3 impulse; // Импульс частицы
};

#endif /* PARTICLE_H_ */
