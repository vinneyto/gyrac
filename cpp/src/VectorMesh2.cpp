#include "VectorMesh2.h"
#include <iostream>
#include <cmath>
#include "fishpack.h"

using namespace std;

VectorMesh2::VectorMesh2(double dx, double dy,
						             double ax, double bx,
						             double ay, double by)
{
	this->set_dx(dx);
	this->set_dy(dy);

	this->set_xrange(ax, bx);
	this->set_yrange(ay, by);

	//Обнуляем указатель
	this->data = NULL;

	reshape();
}

VectorMesh2::~VectorMesh2(void)
{
	delete[] data;
}

void VectorMesh2::reshape()
{
	if (data != NULL)
		delete[] data;

	kx = 1. / dx;
	ky = 1. / dy;

	//Прибавляем +1.5 для округления в большую сторону
	nx = (bx - ax) * kx + 1.5;
	ny = (by - ay) * ky + 1.5;

	data = new Vector3[nx*ny];

	clear();
}

Vector3 VectorMesh2::interp(double x, double y)
{
	// Сеточные координаты
	x = (x - ax) * kx;
  y = (y - ay) * ky;

  // Левый нижний индекс ячейки, в которую попали координаты
	int xi = (int)x;
	int yi = (int)y;

	// Если индекс выходит за пределы массива (с учетом добавки единицы) - возвращаем нулевой вектор
	if (!inRange(xi, 0, nx-1) ||
      !inRange(yi, 0, ny-1)) return Vector3(0., 0., 0.);

  // Вектор - результат интерполяции
	Vector3 res(0, 0, 0);

  // Процедура интерполяции
  // 2 вложенных цикла пробегают по 4-м узлам ячейки, в которую попала координата (x,y)
  // Ячейка разбивается на 4 прямоугольника площади которыйх = abs(xi+1-i-x) * abs(yi+1-j-y)
  // В результат интерполяции последовательно добавляются += значение узла * площадь ПРОТИВОПОЛОЖНОГО прямоугольника
  //
  //   i, j+1 -------- i+1, j+1
  //   |        |        |
  //   |        |        |
  //   |--------|--------|
  //   |        |        |
  //   |        |        |
  //   i, j -----------i+1, j
  //	xi, yi
  //
	for (int j = 0; j < 2; j++)
		for (int i = 0; i < 2; i++)
			res += data[getIndex(xi+i, yi+j)] *
				abs(xi+1-i-x) * abs(yi+1-j-y);

	return res;
}

void VectorMesh2::clear()
{
	int size = nx*ny;
	for (int i = 0; i < size; i++)
		data[i] = Vector3(0, 0, 0);
}
