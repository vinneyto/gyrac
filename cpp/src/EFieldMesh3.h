#ifndef SRC_E_FIELD_MESH_3_h
#define SRC_E_FIELD_MESH_3_h

#include "Vector3.h"
#include <iostream>
#include <vector>
#include <cmath>
#include "Particle.h"
#include "utils.h"

#define EFM3_MIN_DIFF 0.0001

/**
 * Класс для работы с электрическим полем на сетке
 *
 * Логика работы:
 *
 * На скалярную (double) трехмерную сетку наносится заряд
 *
 * На этой же сетке вычисляется потенциал электрического поля
 *
 * Потенциал конвертируется в электрическое поле и наносится на
 * трехмерную векторную (Vector3) сетку
 *
 * С трехмерной сетки производится интерполяция и снятие электрического поля
 */
class EFieldMesh3
{
public:
  EFieldMesh3(double dx, double dy, double dz,
              double ax, double bx,
              double ay, double by,
              double az, double bz);
  ~EFieldMesh3(void);

  /**
   * Возвращает индекс в спрямленном массиве
   * индексы (i,j,k) эквивалентны координатам (x,y,z)
   *
   * @param  i
   * @param  j
   * @param  k
   * @return
   */
  int getIndex(int i, int j, int k)
  {
    return i + j*nx + k*nx*ny;
  }

  bool indexInRange(int i, int j, int k)
  {
    return inRange(i, 0, nx) &&
           inRange(j, 0, ny) &&
           inRange(k, 0, nz);
  }

  float get(int i, int j, int k)
  {
    return indexInRange(i, j, k) ? qphi[getIndex(i, j, k)] : 0;
  }

  /**
   * Возвращает по индексу заряд или потенциал
   * @param  i
   * @param  j
   * @param  k
   * @return
   */
  float get_qphi(int i, int j, int k)
  {
    return get(i, j, k);
  }

  /**
   * Возвращает по индексу электрическое поле
   * @param  i
   * @param  j
   * @param  k
   * @return
   */
  Vector3 get_E(int i, int j, int k)
  {
    return indexInRange(i, j, k) ? efld[getIndex(i, j, k)] : Vector3(0,0,0);
  }

  /**
   * Вычисляет длину вспомогательного массива для решения уравнения пуассона
   * @return
   */
  int calc_wsize() {
    return 30 + nix + niy + 5*niz + fmax(nix, fmax(niy, niz)) +
          7*((int)((nix+1)/2) + (int)((niy+1)/2));
  }

  /**
   * Возвращает суммарный заряд или потенциал на сетке
   * @return
   */
  float get_qphi_total();

  /**
   * Изменяет размеры сеток
   */
  void reshape();

  /**
   * Добавляет заряд на сетку
   * @param v     - координата частицы
   * @param value - разяр
   */
  void append_q(const Vector3 &v, double value);

  /**
   * Добавляет заряд на сетку
   * @param particles - частица
   * @param value     - заряд
   */
  void append_q(std::vector<Particle> &particles, double value);

  /**
   * Возвращает значение электрического поля в точке
   * @param  v [description]
   * @return   [description]
   */
  Vector3 interp_E(const Vector3 &v);

  /**
   * Вычисляет уравнение пуассона на сетке
   */
  void poisson();

  /**
   * Обнуляет сетку
   */
  void clear();

  // Get методы
  int get_nx() { return this->nx; }
  int get_ny() { return this->ny; }
  int get_nz() { return this->nz; }

  double get_ax() { return this->ax; }
  double get_bx() { return this->bx; }
  double get_ay() { return this->ay; }
  double get_by() { return this->by; }
  double get_az() { return this->az; }
  double get_bz() { return this->bz; }

  double get_dx() { return this->dx; }
  double get_dy() { return this->dy; }
  double get_dz() { return this->dz; }

  int get_kx() { return this->kx; }
  int get_ky() { return this->ky; }
  int get_kz() { return this->kz; }

  double get_cell_V() { return this->cell_V; }

  int get_error() { return this->error; }
  float get_computing_error() { return this->computing_error; }

  //Set методы
  void set_dx(double value) {
		this->dx = (value <= EFM3_MIN_DIFF ? EFM3_MIN_DIFF : value);
	}

	void set_dy(double value) {
		this->dy = (value <= EFM3_MIN_DIFF ? EFM3_MIN_DIFF : value);
	}

  void set_dz(double value) {
		this->dz = (value <= EFM3_MIN_DIFF ? EFM3_MIN_DIFF : value);
	}

  void set_xrange(double a, double b) {
    if (a < b) { this->ax = a; this->bx = b; }
    else       { this->ax = b; this->bx = a; }
  }

  void set_yrange(double a, double b) {
    if (a < b) { this->ay = a; this->by = b; }
    else       { this->ay = b; this->by = a; }
  }

  void set_zrange(double a, double b) {
    if (a < b) { this->az = a; this->bz = b; }
    else       { this->az = b; this->bz = a; }
  }

private:

  int nx, ny, nz, nix, niy, niz;
  double ax, bx, ay, by, az, bz;
  double dx, dy, dz, kx, ky, kz;
  double cell_V;
  float* qphi; //Может содержать заряд или потенциал
  Vector3* efld; //Содержит электрическое поле

  // Вспомогательные переменные
  float *w;
  float computing_error;
  int error;
};

#endif
