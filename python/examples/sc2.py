#! /usr/bin/python
# -*- coding: utf-8 -*-

import sys
sys.path.append('..')

import os
import time
import numpy as np
import plasma
import matplotlib
matplotlib.rc('font', family='Arial')

import matplotlib.pyplot as plt

def write_parts(gyr, parts, f):
    for p in parts:
        c = []
        c.append(gyr.conv_xm_x(p.position.x))
        c.append(gyr.conv_xm_x(p.position.y))
        c.append(gyr.conv_xm_x(p.position.z))
        f.write(" ".join(map(str, c)) + "\n")

def write_gir(gyr, f):
    f.write('begin\n')
    f.write('electrons\n')
    write_parts(gyr, gyr.get_electrons(), f)
    f.write('ions\n')
    write_parts(gyr, gyr.get_ions(), f)
    f.write('end\n')

def main():

    conf = plasma.GyracConfig()

    conf.qm_ratio = 1e9 / 1e3

    gyr = plasma.Gyrac(conf)

    gyr.use_sc = True

    R = conf.R
    L = conf.L

    r = R
    h = L

    Q = 1e7 * plasma.EZ * (r**2 * plasma.PI * h)

    Ne = int(Q / gyr.get_qme())

    print 'Ne = %d' % Ne

    plasma.inject_cylinder(plasma.Vector3(0.,0.,0.), gyr.conv_x_xm(h), gyr.conv_x_xm(r), 0, 0, int(Ne/2.), gyr.get_electrons())
    plasma.inject_cylinder(plasma.Vector3(0.,0.,0.), gyr.conv_x_xm(h), gyr.conv_x_xm(r), 0, 0, int(Ne/2.), gyr.get_ions())

    N = 150
    jump = 2000

    print 'processing dump'
    with open('dump.tmp', 'w') as f:
        for i in range(N):
            write_gir(gyr, f)
            gyr.evalute(jump)
            print "write step {}".format(i * jump)

    print '{} frames processed'.format(N)

    print 'rendering dump'
    if not os.path.exists('render'):
        os.makedirs('render')

    with open('dump.tmp', 'r') as f:
        k = 0
        plt.figure(figsize=plt.figaspect(1./2.) * 1)
        for line in f.readlines():

            if line.startswith('begin'):
                plt.clf()
                Ce = [[], [], []]
                Ci = [[], [], []]
            elif line.startswith('el'):
                cur = Ce
            elif line.startswith('io'):
                cur = Ci
            elif line.startswith('end'):
                plt.subplot(1,2,1)
                plt.scatter(Ce[0], Ce[1], c='b')
                plt.scatter(Ci[0], Ci[1], c='r')
                plt.xlim(-R, R)
                plt.ylim(-R, R)
                plt.xlabel('Xe')
                plt.ylabel('Y')
                plt.title('Distr [x,y], step={}'.format(k*jump))
                plt.grid(True)

                plt.subplot(1,2,2)
                plt.scatter(Ce[2], Ce[0], c='b')
                plt.scatter(Ci[2], Ci[0], c='r')
                plt.xlim(-L/2., L/2.)
                plt.ylim(-R, R)
                plt.xlabel('Z')
                plt.ylabel('X')
                plt.title('Distr [z,x], step={}'.format(k*jump))
                plt.grid(True)

                # plt.tight_layout()
                plt.savefig('render/img{}.png'.format(k))

                k += 1

                print '{} frames saved'.format(k)
            else:
                crd = map(float, line.split(" "))
                cur[0].append(crd[0])
                cur[1].append(crd[1])
                cur[2].append(crd[2])


if __name__ == "__main__":
    main()
