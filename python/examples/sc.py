#! /usr/bin/python
# -*- coding: utf-8 -*-

import sys
sys.path.append('..')

import time
import numpy as np
import plasma
import matplotlib
matplotlib.rc('font', family='Arial')

import matplotlib.pyplot as plt

def subplot():
    subplot.counter += 1
    plt.subplot(3,4,subplot.counter)
subplot.counter = 0

def main():

    conf = plasma.GyracConfig()

    conf.qm_ratio = 1e9 / 1e4

    conf.R = 6.0;
    conf.L = 12.0;

    gyr = plasma.Gyrac(conf)

    # самосогласованное электрическое поле будет усаствовать в перемещении частиц
    gyr.use_sc = True

    # радиус
    CR = 1.0

    #Заряд который должен присутствовать на сетке (плотность * элементарный заряд * объем цилиндра электронов)
    Q = 1e9 * plasma.EZ * ( CR**2 * plasma.PI * 2)

    #Заряд шара
    Qs = 1e9 * plasma.EZ * (4./3. * plasma.PI * CR**3)

    print "Q sphere: {}".format(Qs)
    print "Ex(1.0, 0.0, 0.0): {}".format(Qs / gyr.conv_x_xm(1.0)**2)
    print "Ex(1.5, 0.0, 0.0): {}".format(Qs / gyr.conv_x_xm(1.5)**2)
    print "Ex(2.0, 0.0, 0.0): {}".format(Qs / gyr.conv_x_xm(2.0)**2)
    print "Ex(3.0, 0.0, 0.0): {}".format(Qs / gyr.conv_x_xm(3.0)**2)
    print "Ex(4.0, 0.0, 0.0): {}".format(Qs / gyr.conv_x_xm(4.0)**2)
    print "Ex(5.0, 0.0, 0.0): {}".format(Qs / gyr.conv_x_xm(5.0)**2)

    # число модельных частиц
    Ne = int(Q / gyr.get_qme())

    Nes = int(Qs / gyr.get_qme())

    print ('Ne = {}'.format(Ne))
    print ('Nes = {}'.format(Nes))

    #инжектируем цилиндр электронов
    # plasma.inject_cylinder(plasma.Vector3(0,-1.,-1.), gyr.conv_x_xm(1.0), gyr.conv_x_xm(CR), 0, 0, int(Ne/2.), gyr.get_electrons())

    # plasma.inject_cylinder(plasma.Vector3(0, 1., 1.), gyr.conv_x_xm(1.0), gyr.conv_x_xm(CR), 0, 0, int(Ne/2.), gyr.get_electrons())

    # plasma.inject_cylinder(plasma.Vector3(0, 0, 0), gyr.conv_x_xm(2*CR), gyr.conv_x_xm(CR), 0, 0, Ne, gyr.get_electrons())

    plasma.inject_sphere(plasma.Vector3(0, 0, 0), gyr.conv_x_xm(CR), 0, 0, Nes, gyr.get_electrons())

    mesh_E = gyr.get_mesh_E()

    #Добавляем заряд на сетку
    mesh_E.clear()
    mesh_E.append_q(gyr.get_electrons(), -4 * plasma.PI * -1 * gyr.get_qme() / mesh_E.get_cell_V())

    print "total charge on mesh: {}".format(mesh_E.get_qphi_total())

    mesh_E.poisson()

    print "computing_error = {}".format(mesh_E.get_computing_error())
    print "nx/2,ny/2,0 bound = {}".format(mesh_E.get_qphi(mesh_E.get_nx()/2,mesh_E.get_ny()/2,0))

    print "mesh Ex(1.0, 0.0, 0.0): {}".format(mesh_E.interp_E(plasma.Vector3(gyr.conv_x_xm(1.0), 0., 0.)).x)
    print "mesh Ex(1.5, 0.0, 0.0): {}".format(mesh_E.interp_E(plasma.Vector3(gyr.conv_x_xm(1.5), 0., 0.)).x)
    print "mesh Ex(2.0, 0.0, 0.0): {}".format(mesh_E.interp_E(plasma.Vector3(gyr.conv_x_xm(2.0), 0., 0.)).x)
    print "mesh Ex(3.0, 0.0, 0.0): {}".format(mesh_E.interp_E(plasma.Vector3(gyr.conv_x_xm(3.0), 0., 0.)).x)
    print "mesh Ex(4.0, 0.0, 0.0): {}".format(mesh_E.interp_E(plasma.Vector3(gyr.conv_x_xm(4.0), 0., 0.)).x)
    print "mesh Ex(5.0, 0.0, 0.0): {}".format(mesh_E.interp_E(plasma.Vector3(gyr.conv_x_xm(5.0), 0., 0.)).x)

    #Геометрические параметры установки
    R = conf.R
    L = conf.L

    print "kx={}, ky={}, kz={}".format(mesh_E.get_kx(), mesh_E.get_ky(), mesh_E.get_kz())

    # plt.figure(figsize=plt.figaspect(2.5/4.) * 1.5)


    #строим двумерное распределение частиц -----------------------------------------------------------------------------
    x = []
    y = []
    z = []
    for p in gyr.get_electrons():
        x.append(gyr.conv_xm_x(p.position.x))
        y.append(gyr.conv_xm_x(p.position.y))
        z.append(gyr.conv_xm_x(p.position.z))

    # распределение частиц XY
    subplot()
    plt.scatter(x, y)
    plt.xlim(-R, R)
    plt.ylim(-R, R)
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('Distr [x,y]')
    plt.grid(True)

    # распределение частиц ZX
    subplot()
    plt.scatter(z, x)
    plt.xlim(-L/2., L/2.)
    plt.ylim(-R, R)
    plt.xlabel('Z')
    plt.ylabel('X')
    plt.title('Distr [z,x]')
    plt.grid(True)

    # строим контурный график потенциала на сетке ------------------------------------------------------------------------

    nx = mesh_E.get_nx()
    ny = mesh_E.get_ny()
    nz = mesh_E.get_nz()

    print (nx, ny, nz)
    print (mesh_E.get_ax(), mesh_E.get_bx())
    print (mesh_E.get_ay(), mesh_E.get_by())
    print (mesh_E.get_az(), mesh_E.get_bz())

    #строим контур плоскости XY в центре по Z
    x = np.linspace(-R, R, nx)
    y = np.linspace(-R, R, ny)
    X,Y = np.meshgrid(x, y)
    Qxy = np.zeros((ny, nx))
    for j in xrange(ny):
        for i in xrange(nx):
            Qxy[j][i] = mesh_E.get(i, j, int(nz/2.))

    subplot()
    #plt.plot(np.linspace(-R, R, nx), Qxy[ny/2.])
    plt.contour(X, Y, Qxy, 25)
    plt.colorbar()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('Phi(x,y)')

    #строим контур в плоскости ZX в центре по Y
    z = np.linspace(-L/2., L/2., nz)
    x = np.linspace(-R, R, nx)
    Z,X = np.meshgrid(z, x)
    Qzx = np.zeros((nx, nz))
    for i in xrange(nx):
        for k in xrange(nz):
            Qzx[i][k] = mesh_E.get(i, int(ny/2.), k)

    subplot()
    plt.contour(Z, X, Qzx, 25)
    plt.colorbar()
    plt.xlabel('z')
    plt.ylabel('x')
    plt.title('Phi(z,x)')

    # строим график электрического поля -----------------------------------------------------------------------------------

    #строим Ex по x ---

    x = np.linspace(-R, R, nx)
    Ex = np.zeros(nx)

    for i in xrange(nx):
        Ex[i] = mesh_E.get_E(i, int(ny/2.), int(nz/2.)).x

    subplot()
    plt.plot(x, Ex)
    plt.xlabel('x')
    plt.ylabel('Ex')
    plt.title('Ex(x)')
    plt.grid(True)

    #строим Ey по y ---

    y = np.linspace(-R, R, ny)
    Ey = np.zeros(ny)

    for j in xrange(ny):
        Ey[j] = mesh_E.get_E(int(nx/2.), j, int(nz/2.)).y

    subplot()
    plt.plot(y, Ey)
    plt.xlabel('y')
    plt.ylabel('Ey')
    plt.title('Ey(y)')
    plt.grid(True)

    #строим Ez по z ---

    z = np.linspace(-L/2., L/2., nz)
    Ez = np.zeros(nz)

    for k in xrange(nz):
        Ez[k] = mesh_E.get_E(int(nx/2.), int(ny/2.), k).z

    subplot()
    plt.plot(z, Ez)
    plt.xlabel('z')
    plt.ylabel('Ez')
    plt.title('Ez(z)')
    plt.grid(True)

    #интерполируем Ez по z ---

    n = 300

    z = np.linspace(-L/2., L/2., n)
    Ez = np.zeros(n)

    for k in xrange(n):
        Ez[k] = mesh_E.interp_E(plasma.Vector3(0., 0., gyr.conv_x_xm(z[k]))).z

    subplot()
    plt.plot(z, Ez)
    plt.xlabel('z')
    plt.ylabel('Ez')
    plt.title('interp Ez(z) 300 steps')
    plt.grid(True)

    plt.tight_layout()
    plt.show()

if __name__ == "__main__":
    main()
