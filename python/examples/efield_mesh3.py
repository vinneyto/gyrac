#! /usr/bin/python
# -*- coding: utf-8 -*-

import sys
sys.path.append('..')

import plasma

mesh = plasma.EFieldMesh3(
    0.2, 0.2, 0.2, # dx, dy, dz
    -6., 6.,       # ax, bx
    -6., 6.,       # ay, by
    -4., 4.        # az, bz
)

print mesh.get_az()
print mesh.get_bz()

v = plasma.Vector3(0., 0., 0.)

mesh.append_q(v, 100.)

mesh.poisson()

print mesh.get_error()
