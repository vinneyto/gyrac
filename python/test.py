import unittest
import numpy as np
import plasma

def conv_v_u(v):
    return v / plasma.CC

def get_R(v, B):
    return plasma.ME * v * plasma.CC / (plasma.EZ * B)

class TestVectorMesh2(unittest.TestCase):
    def setUp(self):
        self.mesh = plasma.VectorMesh2(0.2, 0.1, -1.0, 2.0, 0.0, 5.0)

    def test_constructor(self):
        self.assertEqual(self.mesh.get_dx(), 0.2)
        self.assertEqual(self.mesh.get_dy(), 0.1)
        self.assertEqual(self.mesh.get_ax(), -1.0)
        self.assertEqual(self.mesh.get_bx(), 2.0)
        self.assertEqual(self.mesh.get_ay(), 0.0)
        self.assertEqual(self.mesh.get_by(), 5.0)
        self.assertEqual(self.mesh.get_nx(), 16)
        self.assertEqual(self.mesh.get_ny(), 51)

    def test_dx_dy_setter(self):
        self.mesh.set_dx(-2.0)
        self.mesh.set_dy(0.0)

        self.assertEqual(self.mesh.get_dx(), plasma.VM2_MIN_DIFF)
        self.assertEqual(self.mesh.get_dy(), plasma.VM2_MIN_DIFF)

    def test_out_of_bounds(self):
        v = self.mesh.interp(10., 10.)

        self.assertEqual(v.x, 0.0)
        self.assertEqual(v.y, 0.0)


class TestEFieldMesh3(unittest.TestCase):
    def setUp(self):
        self.mesh = plasma.EFieldMesh3(
            0.2, 0.2, 0.2, # dx, dy, dz
            -6., 6.,       # ax, bx
            -6., 6.,       # ay, by
            -4., 4         # az, bz
        )

    def test_constructor(self):
        self.assertEqual(self.mesh.get_dx(), 0.2)
        self.assertEqual(self.mesh.get_dy(), 0.2)
        self.assertEqual(self.mesh.get_dz(), 0.2)
        self.assertEqual(self.mesh.get_ax(), -6.)
        self.assertEqual(self.mesh.get_bx(),  6.)
        self.assertEqual(self.mesh.get_ay(), -6.)
        self.assertEqual(self.mesh.get_by(),  6.)
        self.assertEqual(self.mesh.get_az(), -4.)
        self.assertEqual(self.mesh.get_bz(),  4.)
        self.assertEqual(self.mesh.get_nx(), 61)
        self.assertEqual(self.mesh.get_ny(), 61)
        self.assertEqual(self.mesh.get_nz(), 41)

    def test_calc_wsize(self):
        # 30 + 60 + 60 + 40*5 + 60 + 7 * (Math.floor(61/2) + Math.floor(61/2))
        self.assertEqual(self.mesh.calc_wsize(), 830)


class GyracTests(unittest.TestCase):

    def setUp(self):
        conf = plasma.GyracConfig()
        conf.L = 8.0
        conf.R = 6.0
        conf.steps_for_period = 250
        self.gyr = plasma.Gyrac(conf)

    def test_B_radius(self):
        induction = 1000.0  #Gs
        velocity = 1000.0   #sm/s

        self.gyr.use_homogeneous_B = True;
        self.gyr.homogeneous_B.z = induction;

        p = plasma.Particle();
        p.impulse.x = conv_v_u(velocity)
        self.gyr.get_electrons().push_back(p)

        x = []; y = []
        for i in xrange(1000):
            self.gyr.step()
            p = self.gyr.get_electrons()[0].position
            x.append(self.gyr.conv_xm_x(p.x))
            y.append(self.gyr.conv_xm_x(p.y))

        x = np.array(x)
        y = np.array(y)

        r1 = np.abs(x.max() - x.min()) / 2;
        r2 = np.abs(y.max() - y.min()) / 2;

        R = get_R(velocity, induction)

        self.assertAlmostEqual(r1, R, 10)
        self.assertAlmostEqual(r2, R, 10)

    def test_E_distance(self):
        E_strength = 500.0 / 300.0    #V
        t = 3.0 / plasma.HF        #s

        self.gyr.use_homogeneous_E = True;
        self.gyr.homogeneous_E.z = E_strength

        N = int(t * plasma.HF * 250)

        self.gyr.get_electrons().push_back(plasma.Particle())
        self.gyr.evalute(N)

        must_be = -E_strength * plasma.EZ * t**2 / (2.0 * plasma.ME)
        current = self.gyr.conv_xm_x(self.gyr.get_electrons()[0].position.z)

        self.assertAlmostEqual(must_be, current, 2)

if __name__ == '__main__':
    unittest.main()
